(function (Drupal, $) {
    "use strict";
    Drupal.behaviors.geoportailMap = {
        attach: function (context, settings) {

            $('div.geoportail-map', context).once('geoportailMap').each(function () {

                var latlon = settings.geoportail_map.geoportail_map_library.locations;
                var eventPopupData = settings.geoportail_map.geoportail_map_library.event_popup_data;
                //clear received data
                settings.geoportail_map.geoportail_map_library.locations = [];
                
                var bglayer = settings.geoportail_map.geoportail_map_library.bglayer;
                var zoom = settings.geoportail_map.geoportail_map_library.zoom;
                var layersFromDrupal = settings.geoportail_map.geoportail_map_library.layers;
                
                var  layersArray = [];
                if ($.isArray(layersFromDrupal) === false) {
                    layersArray = layersFromDrupal.split(' ');
                }else {
                    layersArray = Object.values(layersFromDrupal);
                }
                //get map center
                var averageLat = 0;
                var averageLon = 0;
                for (var i=0; i < latlon.length; i++){
                  averageLat += latlon[i][1];
                  averageLon += latlon[i][0];
                }
                //center map in luxembourg center when more than x position
                if (latlon.length > 5) {
                    mapCenter = [ 6.091163, 49.781624 ];
                    zoom = 8;
                }else if (latlon.length == 1) {
                    var mapCenter = [averageLat / latlon.length, averageLon / latlon.length];
                    zoom = 14;
                }else{
                    mapCenter = [averageLat / latlon.length, averageLon / latlon.length];
                }
                //display map
                var map = new lux.Map({
                    target: 'map1',
                    bgLayer: bglayer,
                    layers: layersArray,
                    zoom: zoom,
                    positionSrs: 4326, //default EPSG:2169 the luxembourg coordinate reference system, EPSG:4326 the WGS84 coordinate reference system,
                    position: mapCenter,
                });
                //create popup
                function createPopup(popupData){
                    let popupHtml = [];
                    let numberOfDatesOneEvent = 0;
                    for(i=0; i < popupData.length; i++ ){
                        
                        //get more than one dates
                        let dates = '';
                        numberOfDatesOneEvent = popupData[i].events_dates.length;
                        for(var z=0; z < numberOfDatesOneEvent; z++ ){
                            dates += '<time datetime="00Z" class="datetime">'+popupData[i].events_dates[z].startDate+' - '+popupData[i].events_dates[z].endDate+'</time></br>';
                        }
                        numberOfDatesOneEvent = 0;
                        popupHtml[i] = '<div>'+dates+'<strong><a href="'+popupData[i].event_url+'">'+popupData[i].event_title+'</a></strong><br>'+popupData[i].event_place+'</div>';
                    }
                    return popupHtml;
                }
                //show markers on map
                if( typeof eventPopupData == 'undefined') {
                    for (i=0; i < latlon.length; i++){
                      var contentPosition = [latlon[i][1], latlon[i][0]];
                      map.showMarker(
                        {
                          positionSrs: 4326,
                          position: contentPosition,
                          positioning: 'bottom-center',
                          iconURL: '//img.icons8.com/color/48/000000/map-pin.png',
                        });
                    }
                }else{
                    //show markers on map with pop up
                    var displayPopUp = createPopup(eventPopupData);
                    for (i=0; i < latlon.length; i++){
                        contentPosition = [latlon[i][1], latlon[i][0]];
                        map.showMarker(
                        {
                          positionSrs: 4326,
                          position: contentPosition,
                          positioning: 'bottom-center',
                          iconURL: '//img.icons8.com/color/48/000000/map-pin.png',
                          click: true,
                          html: displayPopUp[i],
                        });
                    }  
                }
                //close popup when other popup clicked
                $('div.ol-overlay-container img').on('click', function(){
                    $('button.lux-popup-close').trigger('click');
                    console.log('marker clicked');
                    
                    
                });
            });
        }
    };
}) (Drupal, jQuery);




