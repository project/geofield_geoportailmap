<?php

namespace Drupal\geoportail_map\Plugin\Field\FieldFormatter;

use Drupal\Core\Annotation\Translation;
use Drupal\Core\Field\Annotation\FieldFormatter;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'geoportail_map' formatter.
 *
 * @FieldFormatter(
 *   id = "geoportail_map",
 *   label = @Translation("Geoportail Map"),
 *   field_types = {
 *     "geofield"
 *   }
 * )
 */
class GeoportailMapFormatter extends FormatterBase
{
    /**
     * {@inheritdoc}
     */
    public static function defaultSettings() {
        return [
                'bglayer' => 'basemap_2015_global',
                'layers' => ['addresses', 'cdt_lignes_all'],
                'zoom' => 18,
                'langcode' => 'en',
            ] + parent::defaultSettings();
    }
    /**
     * Helper function to get the baseMap settings options.
     *
     * @return array
     *   The baseMap settings options.
     */
    protected function baseMapOptions() {
      return [
        '556' => $this->t("Road map: basemap_2015_global "),
        '745' => $this->t("Orthophoto 2016: ortho_2016"),
        '502' => $this->t("Topographic map B/W: topo_bw_jpeg"),
        '501' => $this->t("Hybrid map: streets_jpeg"),
      ];
    }
    /**
     * Returns the baseMap, set or default one.
     *
     * @return string
     *   The output format string.
     */
    protected function getBaseMap() {
      return in_array($this->getSetting('bglayer'), array_keys($this->baseMapOptions())) ? $this->getSetting('bglayer') : self::defaultSettings()['bglayer'];
    }
  /**
   * Returns the overlay Layers, set or default one.
   *
   * @return string
   *   The output format string.
   */
  protected function getOverlay() {
    return !empty($this->getSetting('layers')) ? $this->getSetting('layers') : self::defaultSettings()['layers'];
  }
    /**
     * {@inheritdoc}
     */
    public function settingsForm(array $form, FormStateInterface $form_state)
    {
        $elements = parent::settingsForm($form, $form_state);
        $elements['bglayer'] = [
            '#title' => $this -> t('The Id of the background layer'),
            '#type' => 'select',
            '#default_value' => $this->getBaseMap(),
            '#options' => $this->baseMapOptions(),
            '#required' => TRUE,
        ];
        $elements['layers'] = [
            '#type' => 'textfield',
            '#title' => $this-> t('The Id of the overlay layer'),
            '#default_value' => $this->getOverlay(),
            '#description' => $this-> t('Layers to display on top of the base map. <br>Both the ID, or the Technical layer name can be used, separeted with <strong>one space</strong>.<br>Example: addresses cdt_lignes_all 147<br><a href="https://apiv3.geoportail.lu/proj/1.0/build/apidoc/examples/iterate_layers_api.html" target="_blank">List of available layers</a>')
        ];
        $elements['zoom'] = [
            '#type' => 'number',
            '#title' => t('The starting zoom level.'),
            '#default_value' => $this->getSetting('zoom'),
        ];
        return $elements;
    }
    /**
     * {@inheritdoc}
     */
    public function settingsSummary()
    {
        $summary = [];
        $summary[] = t('Background layer: @bglayer', ['@bglayer' => $this->getSetting('bglayer')]);
        $layersFromDefaultOrForm = $this->getSetting('layers');
        if ( is_array($layersFromDefaultOrForm) ) {
          foreach ( $layersFromDefaultOrForm as $key => $oneLayer ) {
            $summary[] = t('Overlay layer: @layers', ['@layers' => $oneLayer ]);
          }
        }else{
          $summary[] = t('Overlay layer: @layers', ['@layers' => $layersFromDefaultOrForm ]);
        }
        $summary[] = t('Starting zoom: @zoom', ['@zoom' => $this->getSetting('zoom')]);
        return $summary;
    }
    /**
     * {@inheritdoc}
     */
    public function viewElements(FieldItemListInterface $items, $langcode)
    {
        $elements = [];
        foreach ($items as $delta => $item) {
            if ($item->isEmpty()) {
                continue;
            }
            $value = $item->getValue();
            if ($value['geo_type'] !== 'Point') {
                continue;
            }
            $locations[] = [$value['lat'],$value['lon']];
            $elements[$delta] = [
              '#theme' => 'geoportail_map',
              '#attached' => [
                  'library' => [
                      'geoportail_map/geoportail_map_library',
                      'geoportail_map/geoportail_map_v3apiloader',
                  ],
                  'drupalSettings' => [
                      'geoportail_map' => [
                          'geoportail_map_library' => [
                              'locations' => $locations,
                              'bglayer' => $this->getSetting('bglayer'),
                              'zoom' => $this->getSetting('zoom'),
                              'layers' => $this->getSetting('layers')
                          ]
                      ],
                  ],
              ],
            ];
        }
        return $elements;
    }
}
