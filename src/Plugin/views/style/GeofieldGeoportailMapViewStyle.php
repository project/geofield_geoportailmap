<?php

namespace Drupal\geoportail_map\Plugin\views\style;

use Drupal\Core\Annotation\Translation;
use Drupal\Core\Entity\Plugin\DataType\EntityAdapter;
use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Annotation\ViewsStyle;
use Drupal\views\Plugin\views\style\DefaultStyle;
use Drupal\views\Views;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Style plugin to render a View output as a geofield map.
 *
 * @ingroup views_style_plugins
 *
 * Attributes set below end up in the $this->definition[] array.
 *
 * @ViewsStyle(
 *   id = "geofield_geoportail_map",
 *   title = @Translation("Geofield Geoportail Map"),
 *   help = @Translation("Displays a View as a Geofield Geoportail Map."),
 *   display_types = {"normal"},
 *   theme = "geoportail-map"
 * )
 */
class GeofieldGeoportailMapViewStyle extends defaultStyle {
  /**
   * {@inheritdoc}
   */
  protected $usesRowPlugin = FALSE;
  /**
   * Does the style plugin support custom css class for the rows.
   *
   * @var bool
   */
  protected $usesRowClass = FALSE;
  /**
   * Does the style plugin support grouping of rows.
   *
   * @var bool
   */
  protected $usesGrouping = FALSE;
  /**
   * Does the style plugin for itself support to add fields to it's output.
   *
   * @var bool
   */
  protected $usesFields = TRUE;
  /**
   * Should field labels be enabled by default.
   *
   * @var bool
   */
  protected $defaultFieldLabels = TRUE;
  /**
   * The list of fields added to the view.
   *
   * @var array
   */
  protected $viewFields = [];
  /**
   * The Entity Field manager service property.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;
  /**
   * The Entity Info Object.
   *
   * @var \Drupal\Core\Entity\EntityTypeInterface
   */
  protected $entityInfo;
  /**
   * The Entity source property.
   *
   * @var string
   */
  protected $entitySource;
  /**
   * The Entity type property.
   *
   * @var string
   */
  protected $entityType;
  /**
   * Options for this plugin will be held here.
   *
   * @var array
   */
  public $options = [];
  /**
   * The Entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityManager;

  /**
   * Set default options.
   */
  protected function defineOptions() {
    $options = parent::defineOptions();

    $options['data_source'] = ['default' => ''];
    $options['entity_source'] = ['default' => '__base_table'];
    $geofield_geoportail_map_default_settings = [];
    foreach (self::getDefaultSettings() as $k => $setting) {
      $geofield_geoportail_map_default_settings[$k] = ['default' => $setting];
    }
    return $options + $geofield_geoportail_map_default_settings;
  }

  /**
   * Helper function to get the baseMap settings options.
   *
   * @return array
   *   The baseMap settings options.
   */
  protected function baseMapOptions() {
    return [
      '556' => $this->t("Road map: basemap_2015_global "),
      '745' => $this->t("Orthophoto 2016: ortho_2016"),
      '502' => $this->t("Topographic map B/W: topo_bw_jpeg"),
      '501' => $this->t("Hybrid map: streets_jpeg"),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    // If data source changed then apply the changes.
    if ($form_state->get('entity_source')) {
      $this->options['entity_source'] = $form_state->get('entity_source');
      $this->entityInfo = $this->getEntitySourceEntityInfo($this->options['entity_source']);
      $this->entityType = $this->entityInfo->id();
      $this->entitySource = $this->options['entity_source'];
    }
    parent::buildOptionsForm($form, $form_state);

    $fields_geo_data = $this->getAvailableDataSources();
    // Check whether we have a geo data field we can work with.
    if (empty($fields_geo_data)) {
      $form['error'] = [
        '#type' => 'html_tag',
        '#tag' => 'div',
        '#value' => $this->t('Please add at least one Geofield to the View and come back here to set it as Data Source.'),
        '#attributes' => [
          'class' => ['geofield-map-warning'],
        ],
      ];
      return;
    }

    // Map data source.
    $form['data_source'] = [
      '#type' => 'select',
      '#title' => $this->t('Data Source'),
      '#description' => $this->t('Which field contains geodata?'),
      '#options' => $fields_geo_data,
      '#default_value' => $this->options['data_source'],
      '#required' => TRUE,
    ];

    $form['bglayer'] = [
      '#title' => $this -> t('The Id of the background layer'),
      '#type' => 'select',
      '#default_value' => $this->options['map_defaults']['bglayer'],
      '#options' => $this->baseMapOptions(),
    ];
    $form['layers'] = [
      '#type' => 'textfield',
      '#title' => $this-> t('The Id of the overlay layer'),
      '#default_value' => $this->options['map_defaults']['layers'],
      '#description' => $this-> t('Layers to display on top of the base map. <br>Both the ID, or the Technical layer name can be used, separeted with <strong>one space</strong>.<br>Example: addresses cdt_lignes_all 147<br><a href="https://apiv3.geoportail.lu/proj/1.0/build/apidoc/examples/iterate_layers_api.html" target="_blank">List of available layers</a>')
    ];
    $form['zoom'] = [
      '#type' => 'number',
      '#title' => t('The starting zoom level.'),
      '#default_value' => $this->options['map_defaults']['zoom'],
    ];
  }
  /**
   * Renders the View.
   */
  public function render() {
    $map_settings = $this->options;
    // Get the Geofield field.
    $geofield_name = $map_settings['data_source']; /* field_geofield  */
    
    /* @var \Drupal\views\ResultRow  $result */
    foreach ($this->view->result as $id => $result) {
        
        if (isset($result->_item)) {
			$item = $result->_item;
			$fields = $item->getFields();

			$nid = $fields['nid']->getValues()[0];
			$entity = \Drupal::entityTypeManager()->getStorage('node')->load($nid);
		} else {
			$entity = $result->_entity;
		}
        //get coordinates
		if ($entity->hasField($geofield_name)) {
			$geofield_value_lat = $entity->get($geofield_name)->lat;
			$geofield_value_lon = $entity->get($geofield_name)->lon;
		}else{
            $geofield_value_lat = $entity->field_location->entity->field_geofield->lat;
            $geofield_value_lon = $entity->field_location->entity->field_geofield->lon;
        }	
        $locations[$id] = [$geofield_value_lat, $geofield_value_lon];
        
        //get data to display on popup
        $eventStartEnd = [];
        if($entity->field_date ){
            $eventDateArray = $entity->field_date->getValue();
            
            foreach( $eventDateArray as $event ){
                $eventStartEnd[] = [
                    'startDate' => date_format(date_create($event['value']), 'l d/m/Y H:i'),
                    'endDate' => date_format(date_create($event['end_value']), 'l d/m H:i')
                ];
            }
        }
        
        if( $entity->field_location ){
            //popup for events
            $eventPopupData[$id] = [
                'events_dates' => $eventStartEnd,
                'event_title' => $entity->title->value,
                'event_url' => $entity->toUrl()->toString(),
                'event_place' => $entity->field_location->entity->title->value
            ];
        }else if ($entity->field_address) {
            //popup for venues
            $eventPopupData[$id] = [
                'events_dates' => $eventStartEnd,
                'event_title' => $entity->title->value,
                'event_url' => $entity->toUrl()->toString(),
                'event_place' => implode(", ", [$entity->field_address->locality, $entity->field_address->address_line1])
            ];
        }else{
          //simple popup
          $eventPopupData[$id] = [
            'events_dates' => $eventStartEnd,
            'event_title' => $entity->title->value,
            'event_url' => $entity->toUrl()->toString(),
            'event_place' => $entity->body->summary,
          ];
        }
        
        
    }
    //create render element
    $element = [
      '#theme' => 'geoportail_map',
      '#attached' => [
        'library' => [
          'geoportail_map/geoportail_map_library',
          'geoportail_map/geoportail_map_v3apiloader',
        ],
        'drupalSettings' => [
          'geoportail_map' => [
            'geoportail_map_library' => [
              'locations' => $locations,
              'bglayer' => $map_settings['bglayer'],
              'zoom' => $map_settings['zoom'],
              'layers' => $map_settings['layers'],
              'event_popup_data' => $eventPopupData
            ]
          ],
        ],
      ],
    ];
    return $element;
  }
  /**
   * Get the Default Settings.
   *
   * @return array
   *   The default settings.
   */
  public static function getDefaultSettings() {
    return [
      'map_dimensions' => [
        'width' => '100%',
        'height' => '450px',
      ],
      'map_defaults' => [
        'bglayer' => '556',
        'zoom' => '9',
        'layers' => '0'
      ]
    ];
  }
  /**
   * Returns the Entity Field manager service property.
   *
   * @return \Drupal\Core\Entity\EntityFieldManagerInterface
   *   The Entity Field manager service property.
   */
  public function getEntityFieldManager() {
    return $this->entityFieldManager;
  }
  /**
   * Get a list of fields and a sublist of geo data fields in this view.
   *
   * @return array
   *   Available data sources.
   */
  protected function getAvailableDataSources() {
    $fields_geo_data = [];

    /* @var \Drupal\views\Plugin\views\ViewsHandlerInterface $handler) */
    foreach ($this->displayHandler->getHandlers('field') as $field_id => $handler) {
	  
      $label = $handler->adminLabel() ?: $field_id;
		
      $this->viewFields[$field_id] = $label;
      if (is_a($handler, '\Drupal\views\Plugin\views\field\EntityField')) {
        /* @var \Drupal\views\Plugin\views\field\EntityField $handler */
        try {
          $entity_type = $handler->getEntityType();
        }
        catch (\Exception $e) {
          $entity_type = NULL;
        }
        $fieldmanager = \Drupal::service('entity_field.manager');
		$field_name = $handler->definition['field_name'];
        $field_storage_definitions = $fieldmanager->getFieldStorageDefinitions($entity_type);
        $field_storage_definition = $field_storage_definitions[$field_name];
        if ($field_storage_definition->getType() == 'geofield') {
          $fields_geo_data[$field_name] = $label;
        }
      }
    }
    return $fields_geo_data;
  }

  /**
   * Get the entity info of the entity source.
   *
   * @param string $source
   *   The Source identifier.
   *
   * @return \Drupal\Core\Entity\EntityTypeInterface
   *   The entity type.
   */
  protected function getEntitySourceEntityInfo($source) {
    if (!empty($source) && ($source != '__base_table')) {
      $handler = $this->displayHandler->getHandler('relationship', $source);
      $data = Views::viewsData();
      if (($table = $data->get($handler->definition['base'])) && !empty($table['table']['entity type'])) {
        try {
          return $this->entityManager->getDefinition($table['table']['entity type']);
        }
        catch (\Exception $e) {
          $entity_type = NULL;
        }
      }
    }
    return $this->view->getBaseEntityType();
  }
}
